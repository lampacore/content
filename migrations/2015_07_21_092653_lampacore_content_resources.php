<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LampacoreContentResources extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('lampacore_content_resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_sign');
            $table->unsignedInteger('parent_id')->nullable();

            $table->string('slug');
            $table->unsignedInteger('position')->nullable();

            $table->string('status')->nullable();

            $table->timestamps();

            $table->text('name');
            $table->text('exc');
            $table->text('content');
            $table->text('content2');
            $table->text('content3');
            $table->text('content4');
            $table->unsignedInteger('price');
            $table->timestamp('date');
            $table->text('link');

            $table->text('extras')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('lampacore_content_resources');
    }
}

<?php

namespace Lampacore\Content;

use Carbon\Carbon;
use Event;
use Illuminate\Routing\Controller;
use Input;
use Redirect;
use View;

class ResourceController extends Controller
{

    public function getList($groupSlug, $parentID = null)
    {

        $group = new Group($groupSlug);

        $resourceQuery = Resource::query();

        $resourceQuery->whereGroupSign($groupSlug);

        if ($parentID) {

            $resourceQuery->where('parent_id', $parentID);
            $parentResource = Resource::find($parentID);
        } else {
            $parentResource = new Resource;
        }

        if (!$parentResource) {
            return Redirect::back();
        }

        if ($group->get('list_order')) {

            call_user_func_array([ $resourceQuery, 'orderBy' ], $group->get('list_order'));

        } else {
            $resourceQuery->positionOrder();
        }

        $resourceList = $resourceQuery->get();

        $group = new Group($groupSlug);

        Event::fire('light.resource.'. $groupSlug);

        Event::fire('content.resource.list', [ $groupSlug, $resourceList ]);

        $view = ($group->get('view_list') ? $group->get('view_list') : 'content::resource.list');

        return View::make($view,
            [ 'resourceList' => $resourceList, 'group' => $group, 'parentResource' => $parentResource ]);
    }

    public function getSingle($groupSlug)
    {
        $resource = Resource::whereGroupSign($groupSlug)->first();
        /* @type \Content\Resource $resource */

        Event::fire('light.resource.'. $groupSlug);

        if ($resource) {
            return \Redirect::action('\\'.__CLASS__ . "@getEdit", $resource->id);
        } else {
            return \Redirect::action('\\'.__CLASS__ . "@getCreate", $groupSlug);
        }
    }

    public function getCreate($groupSlug, $parentID = null)
    {
        $resource = new Resource([ 'group_sign' => $groupSlug ]);
        /* @type \Content\Resource $resource */

        $resource->parent_id = $parentID;

        $group = $resource->group();

        Event::fire('light.resource.'. $groupSlug);

        return View::make('content::resource.edit', [ 'resource' => $resource, 'group' => $group, ]);
    }

    public function getEdit($resourceID)
    {
        $resource = Resource::findOrFail($resourceID);
        /* @type \Lampacore\Content\Resource $resource */

        $group = $resource->group();

        Event::fire('light.resource.'. $group->getSlug());

        $view = ($group->get('view_edit') ? $group->get('view_edit') : 'content::resource.edit');

        return View::make($view, [ 'resource' => $resource, 'group' => $group, ]);
    }

    public function postEdit($resourceID = null)
    {

        $resource = ($resourceID) ? Resource::findOrFail($resourceID) : new Resource;
        /* @type \Content\Resource $resource */

        $resource->fill(Input::all());

        if (!Input::get('date')) {
            //$resource->date = Carbon::now();
        }

        if ($resource->group()->get('extras')) {
            $resource->setExtrasFromInput();
        }

        $resource->save();

        #

        if ($resource->group()->get('related')) {
            $resource->directRelation()->sync((array) Input::get('related'));
        }

        # supervise
        \Event::fire('supervise', [
            ($resourceID ? 'edit' : 'create'),
            ($resource->group()->get('single') ? null : $resource->group()),
            $resource
        ]);

        if (Input::get('redirect') == 'list') {
            $r = Redirect::action('\\'.__CLASS__ . "@getList", [ $resource->group_sign ]);
        } else {
            $r = Redirect::action('\\'.__CLASS__ . "@getEdit", [ $resource->id ]);
        }

        $r->with('msg', 'msg.edit');

        return $r;

    }

    public function getDelete($resourceID)
    {
        $resource = Resource::findOrFail($resourceID);

        /* @type \Content\Resource $resource */

        $groupSlug = $resource->group_sign;

        $group = $resource->group();

        $parentID = $resource->parent_id;

        $resource->delete();

        # supervise
        \Event::fire('supervise', [ 'delete', $group, $resource ]);

        return Redirect::action('\\'.__CLASS__ . "@getList", [ $groupSlug, $parentID ])->with('msg', 'msg.delete');
    }

}

<?php

namespace Lampacore\Content;

class Group
{

    protected $slug;

    # Slug set\get

    public function __construct($slug)
    {
        $this->setSlug($slug);
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    #

    public function getStatusStyle(Resource $object){
        return $this->get('statuses')[$object->status]['style'];
    }

    public function getStatusName(Resource $object)
    {
        return $this->get('statuses')[$object->status]['name'];
    }
    #

    public function fieldName($field, $default = null)
    {
        if ($this->get('field_names.' . $field)) {
            return $this->get('field_names.' . $field);
        } else {
            return $default;
        }
    }

    public function get($key = null)
    {

        if ($key !== null) {
            return \Config::get("content_groups." . $this->slug . "." . $key);
        } else {
            return \Config::get("content_groups." . $this->slug);
        }

    }

    public function hasField($key)
    {
        return in_array($key, (array) $this->get('fields'));
    }

    # for supervision

    public function svTitle()
    {
        return $this->get('name');
    }

    public function childSlugs()
    {

        $childSlugs = [ ];

        foreach (\Config::get('content_groups') as $groupSlug => $groupData) {
            if (($parent = array_get($groupData, 'parent', null)) && $parent == $this->getSlug()) {
                $childSlugs[] = $groupSlug;
            }
        }
        return $childSlugs;

    }


    public function firstChildSlug(){
        return (isset($this->childSlugs()[0])) ?  $this->childSlugs()[0] : null;
    }

    public function childs(){

        $childs = [];

        foreach ($this->childSlugs() as $childSlug) {

                $childs[] = new Group($childSlug);

        }
        return $childs;

    }


    public function parent(){
        return new static($this->get('parent'));
    }


    public function related(){
        $related = [];

        foreach((array) $this->get('related') as $slug){
            $related[] = new Group($slug);
        }

        return $related;
    }


    public function resources($getResult = false){
        $resQuery = Resource::whereGroupSign($this->slug)->orderBy('id', 'desc');

        if($getResult){
            return $resQuery->get();
        }
        else{
            return $resQuery;
        }
    }

}

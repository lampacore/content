<?php

namespace Lampacore\Content;

use Illuminate\Support\ServiceProvider;

class LampacoreContentServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {


        /*
        * Migrations
        */

        $this->publishes([ __DIR__ . '/../migrations' => database_path('migrations') ], 'migrations');

        /*
         * Helpers
         */

        require_once __DIR__ . '/helpers.php';

        /*
         * Views
         */
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'content');

        /*
         * Assets
         */

        $this->publishes([ __DIR__ . '/../public' => public_path('packages/lampacore/content') ], 'assets');

        /*
         * Routes
         */

        \Route::controller('/admin/content/resource', ResourceController::class,
            [
                'getList' => 'lampacore.content.resource.list',
                'getEdit' => 'lampacore.content.resource.edit',
                'getSingle' => 'lampacore.content.resource.single',
            ]
        );
    }
}

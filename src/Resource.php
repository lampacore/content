<?php

namespace Lampacore\Content;

use Carbon\Carbon;
use Lampacore\Extras\ExtrasTrait;
use Lampacore\Positions\PositionsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Lampacore\Images\ImagesTrait;
use Stringy\StaticStringy as S;

/**
 * Class \Content\Resource
 * @package Content
 * @method Builder|\Lampacore\Content\Resource groupAndSlug(string $group, string $slug)
 * @method Builder|\Lampacore\Content\Resource whereGroupSign(string $group)
 * @method Builder|\Lampacore\Content\Resource whereParentId(int $id)
 */
class Resource extends \Eloquent
{

    /**
     * @type bool
     */
    public $timestamps = true;

    /**
     * @type Group
     */
    protected $_group;

    /**
     * @type string|null
     */
    protected $_url;

    /**
     * @type string
     */
    protected $table = 'lampacore_content_resources';

    /**
     * @type array
     */
    protected $fillable = [
        'group_sign',
        'parent_id',
        'name',
        'exc',
        'slug',
        'content',
        'content2',
        'content3',
        'content4',
        'price',
        'link',
        'date',
        'status'

    ];

    /**
     * @type array
     */
    protected $appends = [];

    /**
     * @type static
     */
    protected $_parent;

    /**
     * @type EloquentCollection
     */
    protected $_ancestors;

    use ImagesTrait;
    use PositionsTrait;
    use ExtrasTrait;

    /**
     * @param static|null $instance
     *
     * @return static
     */
    public static function object($instance)
    {
        if ($instance instanceof static) {
            return $instance;
        } else {
            return new static;
        }
    }

    /**
     * @param $value
     *
     * @throws \InvalidArgumentException
     */
    public function setGroupSignAttribute($value)
    {
        if (!\Config::get('content_groups.' . $value)) {
            throw new \InvalidArgumentException("Group $value doesnt exists");

        }

        $this->attributes['group_sign'] = $value;

    }

    public function excerpt($len = 180, $ending = '...')
    {
        return S::safeTruncate(strip_tags($this->content), $len, $ending);
    }

    public function setSlugAttribute($slug)
    {
        $slug = S::slugify($slug);

        if (!$slug) {
            $this->attributes['slug'] = S::slugify($this->name);
        } else {
            $this->attributes['slug'] = $slug;
        }
    }

    /**
     * @return int|bool
     */
    public function imagesMax()
    {
        return $this->group()->get('images_max');
    }

    /**
     * @return Group
     */
    public function group()
    {
        if (!isset($this->_group)) {
            $this->_group = new Group($this->group_sign);
        }

        return $this->_group;
    }

    /**
     * @return array
     */
    public function getExtraFields()
    {
        return (array)$this->group()->get('extras');
    }

    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'date'];
    }

    /**
     * @return string
     */
    public function title()
    {
        if ($this->group()->get('single')) {
            return $this->group()->get('name');
        }

        if ($this->name) {
            return $this->name;
        }

        if ($this->exc || $this->content) {
            return $this->excerpt(20);
        }

        if ($this->name) {
            return $this->name;
        }

        return $this->group()->get('name') . " #" . $this->id;
    }

    protected function getAvailableUrlParams()
    {
        $vars = [];

        $vars = array_merge($vars, $this->toArray());

        $vars['group__slug'] = $this->group()->getSlug();

        if ($this->parent()) {

            $group_slug = $this->parent()->group()->getSlug();

            foreach ($this->parent()->toArray() as $attr => $value) {
                $vars["{$group_slug}__{$attr}"] = $value;
            }
        }

        return $vars;
    }

    /**
     * @return null|string
     */
    public function url()
    {

        if ($this->link) {
            return $this->link;
        }

        if (!$this->group()->get('action')) {
            return null;
        }

        $actionKey = S::removeLeft($this->group()->get('action'), '\\');

        $route = \Route::getRoutes()->getByAction($actionKey);

        //dd(\Route::getRoutes());

        //dd($route);
        if (!$route) {
            return null;
        }

        //$url = $route->getUri();

        $parameters = [];

        foreach ($route->parameterNames() as $parameterName) {

            if (isset($this->getAvailableUrlParams()[$parameterName])) {
                $parameters[] = $this->getAvailableUrlParams()[$parameterName];
            }

        }

        if (!isset($this->_url)) {
            $this->_url = action($this->group()->get('action'), $parameters);
        }

        return $this->_url;

    }

    /**
     * @param string $orderField
     *
     * @return Builder
     */
    public function previous($orderField = 'id')
    {
        return static::whereGroupSign($this->group_sign)->where($orderField, '>',
            $this->$orderField)->orderBy($orderField,
            'asc')->take(1)->first();
    }

    /**
     * @param string $orderField
     *
     * @return Builder
     */
    public function next($orderField = 'id')
    {
        return static::whereGroupSign($this->group_sign)->where($orderField, '<',
            $this->$orderField)->orderBy($orderField,
            'desc')->take(1)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function directRelation()
    {
        return $this->belongsToMany('Content\Resource', 'content_resource_rel', 'relator_id', 'related_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function inverseRelation()
    {
        return $this->belongsToMany('Content\Resource', 'content_resource_rel', 'related_id', 'relator_id');
    }

    /**
     * @return mixed|Resource|null
     */
    public function parent()
    {
        if (!$this->group()->get('parent')) {
            return null;
        }

        if (!isset($this->_parent)) {

            $parentQuery = $this->whereGroupSign($this->group()->get('parent'))->where('id', $this->parent_id);

            $this->_parent = $parentQuery->first();

            if (!$this->_parent) {
                $this->_parent = new static;
            }
        }

        return $this->_parent;
    }

    /**
     * @param string $groupSign
     *
     * @return Resource|null
     */
    public function ancestor($groupSign)
    {
        if (!isset($this->_ancestors[$groupSign])) {
            $this->_ancestors[$groupSign] = $this->parent();

            while ($this->_ancestors[$groupSign] && $this->_ancestors[$groupSign]->group_sign != $groupSign) {
                $this->_ancestors[$groupSign] = $this->_ancestors[$groupSign]->parent();
            }
        }

        return $this->_ancestors[$groupSign];
    }

    /**
     * @param $q
     * @param $groupSlug
     * @param $slug
     *
     * @return Builder
     */
    public function scopeGroupAndSlug($q, $groupSlug, $slug)
    {
        return $q->where('group_sign', $groupSlug)->where('slug', $slug);
    }

    /**
     * @param bool $withSelf
     *
     * @return Collection
     */
    public function parentChain($withSelf = false)
    {

        $parentCollection = new Collection;
        $parentID         = $this->parent_id;
        while ($parentID) {

            $parent = Resource::find($parentID);
            $parentCollection->push($parent);
            $parentID = $parent->parent_id;
        }

        $parentCollection = $parentCollection->reverse();

        if ($withSelf) {
            $parentCollection->push($this);
        }

        return $parentCollection;

    }

    /**
     * @param string $groupSlug
     * @param bool $getResult
     *
     * @return Builder|\Illuminate\Database\Eloquent\Collection
     */
    public function childs($groupSlug, $getResult = false)
    {

        $childsQuery = $this->where('group_sign', $groupSlug)->where('parent_id', $this->id);

        if ($getResult) {
            return $childsQuery->get();
        } else {
            return $childsQuery;
        }

    }

    /**
     * @param $childSlugs
     * @param null $relatedSlug
     *
     * @return $this|\Illuminate\Database\Eloquent\Relations\Relation
     */
    public function deep($childSlugs, $relatedSlug = null)
    {

        $childSlugs = explode('.', $childSlugs);

        $qKey = (new static)->getQualifiedKeyName();

        $resourceIds = [$this->id];

        foreach ($childSlugs as $c => $slug) {

            //print_r($resourceIds);

            $childsQuery = Resource::where('group_sign', $slug);

            if ($resourceIds) {
                $childsQuery->whereIn('parent_id', $resourceIds);
            } else {
                break;
            }

            $resourceIds = $childsQuery->lists('id');

        }

        if ($relatedSlug) {

            if (!$resourceIds) {
                $resourceIds = [null];
            }

            $relatedQuery = Resource::query()->getRelation('directRelation');

            $relatedQuery->where('group_sign', $relatedSlug);

            $relatedQuery->whereIn('relator_id', $resourceIds)->groupBy($qKey);

            return $relatedQuery;
        } else {
            return $childsQuery;
        }

    }

    /**
     * @param $slugs
     * @param bool $firstIsRelator
     *
     * @return Builder
     */
    public function above($slugs, $firstIsRelator = false)
    {

        $slugs = explode('.', $slugs);

        $parents = Resource::query();

        foreach ($slugs as $c => $slug) {
            if ($c === 0 && $firstIsRelator) {

                $parents = $this->relators($slug);

            } else {

                if ($parentIds = $parents->lists('parent_id')) {
                    $parents = Resource::whereIn('id', $parentIds);
                } else {
                    $parents = Resource::query();
                }

            }
        }

        return $parents;
    }

    /**
     * @param string $groupSlug
     * @param bool $getResult
     *
     * @return EloquentCollection|Builder
     */
    public function related($groupSlug, $getResult = false)
    {

        if (!$this->group()->get('related')) {
            return new Collection;
        }

        $relatedQuery = $this->directRelation()->where('group_sign', $groupSlug);

        if ($getResult) {
            return $relatedQuery->get();
        } else {
            return $relatedQuery;
        }

    }

    /**
     * @param string $groupSlug
     * @param bool $getResult
     *
     * @return EloquentCollection|Builder
     */
    public function relators($groupSlug, $getResult = false)
    {
        if (!$this->group()->get('relators')) {
            return new Collection;
        }

        $relatedQuery = $this->inverseRelation()->where('group_sign', $groupSlug);

        if ($getResult) {
            return $relatedQuery->get();
        } else {
            return $relatedQuery;
        }
    }

    public function deleteSelfIfNoParent()
    {
        if ($this->parent_id && $this->group()->get('parent') && !static::find($this->parent_id)) {
            $this->delete();
        }
    }

    public function deleteChilds()
    {
        //die('111');
        Resource::where('parent_id', $this->id)->delete();
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s');
    }

}

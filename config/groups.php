<?php

return [

    'blog_cat'     => [

        'name'        => 'Категория записей блога',
        'name_plural' => 'Категории записей блога',
        'fields'      => [ 'name', 'slug' ],

    ],
    'blog_article' => [

        'name'        => 'Запись в блоге',
        'name_plural' => 'Записи в блоге',
        'fields'      => [ 'name', 'slug', 'exc', 'content' ],
        'parent'      => 'blog_cat',

    ],
    /*
        'lead'           => [

            'permission'  => 'lead.*',
            'name'        => 'Заявка из формы',
            'name_plural' => 'Заявки из формы',
            'view_edit'   => 'content::resource.show_with_status',
            'view_list'   => 'packages.content.resource.lead_list',
            'extras'      => [

                'fio'   => [ 'name' => 'ФИО' ],
                'count' => [ 'name' => 'Кол-во человек' ],
                'phone' => [ 'name' => 'Телефон' ],
                'age'   => [ 'name' => 'Возраст' ],
                'url'   => [ 'name' => 'Источник', 'type' => 'url' ],

            ],
            'fields'      => [ 'status', 'date', 'content' ],
            'statuses'    => [

                'new'      => [ 'name' => 'Новая', 'style' => 'success' ],
                'callback' => [ 'name' => 'Перезвон', 'style' => 'warning' ],
                'done'     => [ 'name' => 'Обработана', 'style' => 'default' ],

            ]
        ],
        'country'        => [

            'permission'  => 'content.resource.*',
            'name'        => 'Страна',
            'name_plural' => 'Страны',
            'fields'      => [ 'name', 'content' ],
            'action'      => 'Timescenter\ShowcaseController@getCountry',
            'extras'      => [
                'currency' => [ 'name' => 'Валюта' ]
            ],
            'images'      => [

                'country_main'    => [
                    'title'     => 'Основное изображение',
                    'imagesMax' => 1
                ],
                'country_lead'    => [
                    'title'     => 'Лид-форма',
                    'imagesMax' => 1
                ],
                'country_gallery' => [
                    'title'     => 'Галерея',
                    'imagesMax' => null
                ],

            ],
            'list_order'  => [ 'name', 'asc' ]

        ],
        'city'           => [

            'permission'  => 'content.resource.*',
            'name'        => 'Город',
            'name_plural' => 'Города',
            'fields'      => [ 'name', 'content', 'slug' ],
            'images'      => true,
            'parent'      => 'country',
            'action'      => 'Timescenter\ShowcaseController@getCity',
            'list_order'  => [ 'name', 'asc' ]
        ],
        'school'         => [

            'permission'  => 'content.resource.*',
            'name'        => 'Школа',
            'name_plural' => 'Школы',
            'fields'      => [ 'name', 'exc', 'content', 'slug' ],
            'parent'      => 'city',
            'action'      => 'Timescenter\ShowcaseController@getSchool',
            'list_order'  => [ 'name', 'asc' ],
            'images'      => [

                'school_main'    => [
                    'title'     => 'Основное изображение',
                    'imagesMax' => 1
                ],
                'school_lead'    => [
                    'title'     => 'Лид-форма',
                    'imagesMax' => 1
                ],
                'school_gallery' => [
                    'title'     => 'Галерея',
                    'imagesMax' => null
                ],
            ]
        ],
        'course'         => [
            'permission'  => 'content.resource.*',
            'name'        => 'Курс',
            'name_plural' => 'Курсы',
            'fields'      => [ 'name' ],
            'images'      => false,
            'parent'      => 'school',
            'hide_parent_select' => true,
            'related'     => [ 'course_type', 'lang' ],
            'action'      => 'Timescenter\ShowcaseController@getCourse',
            'extras'      => [
                'time' => [ 'name' => 'Длительность курса' ],

            ],
            'list_order'  => [ 'name', 'asc' ]
        ],
        'lang'           => [

            'name'        => 'Язык',
            'name_plural' => 'Языки',
            'fields'      => [ 'name', 'content', 'slug' ],
            'images'      => [

                'lang_main' => [
                    'title'     => 'Основное изображение',
                    'imagesMax' => 1
                ],
                'lang_lead' => [
                    'title'     => 'Лид-форма',
                    'imagesMax' => 1
                ],
            ],
            'relators'    => [ 'course' ],
            'action'      => 'Timescenter\ShowcaseController@getLang',
            'list_order'  => [ 'name', 'asc' ]

        ],
        'course_type'    => [

            'permission'  => 'content.resource.*',
            'name'        => 'Тип курса',
            'name_plural' => 'Типы курсов',
            'fields'      => [ 'name', 'content', 'slug' ],

            'relators'    => [ 'course' ],
            'action'      => 'Timescenter\ShowcaseController@getCourseType',
            'list_order'  => [ 'name', 'asc' ],


            'images'      => [

                'ct_main'    => [
                    'title'     => 'Основное изображение',
                    'imagesMax' => 1
                ],
                'ct_lead'    => [
                    'title'     => 'Лид-форма',
                    'imagesMax' => 1
                ],
                'ct_gallery' => [
                    'title'     => 'Галерея',
                    'imagesMax' => null
                ],
            ]

        ],
        'cert'           => [

            'permission'  => 'content.resource.*',
            'name'        => 'Сертификат',
            'name_plural' => 'Сертификаты',
            'fields'      => [ 'name' ],
            'images'      => true,
            'images_max'  => 1,

        ],
        'abroad' => [
            'permission'  => 'content.resource.*',
            'name_plural' => 'Обучение за рубежом',
            'name'        => 'Страница цикла Обучение за рубежом',
            'fields'      => [ 'name', 'slug', 'content' ],
            'action'      => 'Timescenter\ShowcaseController@getPage',
            'sort'        => true,
            'images'      => true,
            'images_max'  => 1
        ],
        'courses_static' => [
            'permission'  => 'content.resource.*',
            'name_plural' => 'Курсы',
            'name'        => 'Страница о курсах',
            'fields'      => [ 'name', 'slug', 'content' ],
            'action'      => 'Timescenter\ShowcaseController@getPage',
            'sort'        => true,
            'images'      => true,
            'images_max'  => 1
        ],
        'blog' => [
            'permission'  => 'content.resource.*',
            'name_plural' => 'Блог',
            'name'        => 'Запись в блоге',
            'fields'      => [ 'name', 'slug', 'content','date' ],
            'action'      => 'Timescenter\ShowcaseController@getPage',
            'sort'        => true,
            'images'      => true,
            'images_max'  => 1
        ],

        'exam'           => [
            'single' => true,
            'name'   => 'Контакты',
            'fields' => [ 'name', 'slug', 'content' ],
            'action' => 'Timescenter\ShowcaseController@getSinglePage'

        ],
        'banner'         => [

            'name'        => 'Баннер',
            'name_plural' => 'Баннеры',
            'images'      => true,
            'images_max'  => 1,
            'fields'      => [ 'link' ],
            'view_list'   => 'content::resource.list.banners',

        ],

        'top_banner'           => [
            'single' => true,
            'name'   => 'Баннер в шапке',
            'fields' => [ 'link'],
            'images'      => [

                'top_banner' => [
                    'title'     => 'Баннер 378x72',
                    'imagesMax' => 1
                ],

            ]
        ]
    */

];

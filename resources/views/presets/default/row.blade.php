<tr object-id="{{$resource->id}}">

    @if($group->hasField('name'))
        <td class="resource_name">{{$resource->name}}
            {{--@hook('content::list.name', compact('resource','group'))--}}
        </td>
    @endif


    {{--    <div class="col-md-1">

                @foreach($resource->images()->take(1) as $iImage)
                    <img src="{{$iImage->url('100x100')}}" alt="" class="thumbnail" style="margin-bottom:2px;"/>
                @endforeach

        </div>--}}

    @if($group->childSlugs())

        <td>

            @foreach($group->childs() as $childGroup)

                <a href="{{action('\Lampacore\Content\ResourceController@getList',[ $childGroup->getSlug(), $resource->id])}}">{{$childGroup->get('name_plural')}}
                    ({{$resource->childs($childGroup->getSlug())->count()}})</a>

            @endforeach

        </td>

    @endif


    <td class="text-center">
        @if($resource->url())
            <a href="{{ $resource->url() }}" target="_blank" class="btn btn-xs btn-primary">
                <span class="glyphicon glyphicon-eye-open"></span>
            </a>
        @endif
        <a href="{{ action('\Lampacore\Content\ResourceController@getEdit', array($resource->id)) }}"
           class="btn btn-xs btn-primary">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>
        <a href="{{ action('\Lampacore\Content\ResourceController@getDelete', array($resource->id)) }}"
           class="btn btn-xs btn-danger">
            <span class="glyphicon glyphicon-trash"></span>
        </a>
        @if($group->get('sort'))
            <span class="btn btn-xs btn-warning item-drag" style="cursor: move"><span
                        class="glyphicon glyphicon-sort glyphicon-white"></span></span>
        @endif
    </td>

</tr>
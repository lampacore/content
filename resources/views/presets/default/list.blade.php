<script type="text/javascript">
    $(function(){
               
        $('#resource_search').on('keyup', function(e){
            e.preventDefault();
            
            var needle = $(this).val();
            
            var needleRX = new RegExp(needle,"i");
            //alert(needleRX);
    
            $(".resource_name").parent().show();
            
            $(".resource_name").filter(function(i){
                
               // alert($(this).text());
                
                return ($(this).text().search(needleRX) === -1);
                
            }).parent().hide();
            
        });
        
    });
</script>

<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <input type="text" id="resource_search" class="form-control" placeholder="поиск"/>
    </div>
</div>

<br>

<table class="table table-bordered" style="width: auto !important;">

    <tbody id="sortable-items-container">

    @foreach($resourceList as $resource)

        @if(View::exists('admin.content.'.$group->getSlug().'.row'))
            @include('admin.content.'.$group->getSlug().'.row')
        @else
            @include('content::presets.default.row')
        @endif

    @endforeach

    </tbody>

</table>
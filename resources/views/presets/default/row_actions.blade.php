@if($resource->url())
    <a href="{{ $resource->url() }}" target="_blank" class="btn btn-xs btn-default">
        <span class="glyphicon glyphicon-eye-open"></span>
    </a>
@endif
<a href="{{ action('\Lampacore\Content\ResourceController@getEdit', array($resource->id)) }}"
   class="btn btn-xs btn-primary">
    <span class="glyphicon glyphicon-pencil"></span>
</a>
<a href="{{ action('\Lampacore\Content\ResourceController@getDelete', array($resource->id)) }}"
   class="btn btn-xs btn-danger">
    <span class="glyphicon glyphicon-trash"></span>
</a>
@if($group->get('sort'))
    <span class="btn btn-xs btn-warning item-drag" style="cursor: move"><span
                class="glyphicon glyphicon-sort glyphicon-white"></span></span>
@endif

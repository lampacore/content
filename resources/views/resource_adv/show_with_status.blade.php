@extends('panel::master')

@section('scripts')

<script type="text/javascript" src="{{moduleAsset('panel','js/ckeditor/ckeditor.js')}}"></script>

@stop

@section('breadcrumbs')

    @if(!$group->get('single'))

        @foreach($resource->parentChain() as $parent)

            @crumb($parent->title(), 'Content\ResourceController@getList', [$parent->group_sign, $parent->parent_id])

        @endforeach

            @crumb($group->get('name_plural'), 'Content\ResourceController@getList', [$group->getSlug(), $resource->parent_id])
    @endif

    @if($resource->exists)

        @crumb($resource->title(), URL::current())

    @else
        @crumb($group->get('name'), URL::current())
    @endif

@stop

@section('content')

<h1>

    @if($resource->exists)
        {{$resource->title()}}
    @else
        {{$group->get('name')}}
    @endif
<span class="label label-{{$group->getStatusStyle($resource)}}">{{$group->getStatusName($resource)}}</span>
</h1>

<hr/>



<form action="{{ action('Content\ResourceController@postEdit', array($resource->id)) }}" method="post">
<div class="row">

    <div class="col-md-6 col-lg-8">

            @hook('content::resource.edit', compact('resource','group'))


            @if($group->hasField('date'))

                <div class="form-group">
                    <label for="date">{{$group->fieldName('date', 'Дата и время')}}</label>

                    <p>{{$resource->date}}</p>
                </div>

            @endif


            @if($group->get('extras'))
                @include('abstract_content::extras.show', ['object' => $resource, 'key' => "content::groups.{$resource->group_sign}.extras"])
            @endif


            @if($group->hasField('content'))

                <div class="form-group">
                    <label for="date">Комментарий</label>

                    <textarea name="content" id="" rows="10" class="form-control">{{$resource->content}}</textarea>
                </div>

            @endif

            @if($group->hasField('status'))

                @foreach($group->get('statuses') as $statusSlug => $statusData)

                <button type="submit" name="status" value="{{$statusSlug}}" class="btn btn-{{$statusData['style']}}">{{$statusData['name']}}</button>

                @endforeach


            @endif

            <a class="btn btn-danger" href="{{action('Content\ResourceController@getDelete', $resource->id)}}"><span class="glyphicon glyphicon-trash"></span></a>

    </div>


    <div class="col-md-6 col-lg-4">

    </div>


</div>
</form>


@stop

@extends('panel::master')

@section('breadcrumbs')

        @if($parentResource->exists)
            @foreach($parentResource->parentChain(true) as $parent)

                @crumb($parent->title(), 'Content\ResourceController@getList', [$parent->group_sign, $parent->parent_id])

            @endforeach
        @endif

            @crumb($group->get('name_plural'), 'Content\ResourceController@getList', [$group->getSlug(), $parentResource->parent_id])


@stop

@section('content')


<script type="text/javascript">
        var tableForSort = '{{ (new Content\Resource)->getTable() }}'
    </script>
<script type="text/javascript" src="{{ moduleAsset('abstract_content', 'js/sort.js')}}"></script>

<h1 class="page-header">
    {{$group->get('name_plural')}} <a href="{{ action('Content\ResourceController@getCreate', [$group->getSlug(), $parentResource->id]) }}" class="btn btn-success">добавить</a>
</h1>


    <div id="sortable-items-container" class="row">

    @foreach($resourceList as $iResource)

    <div class="col-md-3" object-id="{{$iResource->id}}">

    <div class="panel panel-default">


        <div class="panel-heading item-drag">

        {{$iResource->link}}

        </div>


        <div class="panel-body">

            <img class="img-thumbnail" src="{{$iResource->firstImage()->url('400x200')}}" alt=""/>

        </div>


        <div class="panel-footer text-center">
            <a href="{{ action('Content\ResourceController@getEdit', array($iResource->id)) }}" class="btn btn-xs btn-primary">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a href="{{ action('Content\ResourceController@getDelete', array($iResource->id)) }}" class="btn btn-xs btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
            </a>

        </div>

    </div>


    </div>


    @endforeach

    </div>

@stop
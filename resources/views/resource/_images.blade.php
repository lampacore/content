@if($images === true)

<div class="row">
    <div class="col-md-6 col-lg-8">

        @include('images::gallery', ['owner' => $resource, 'viewOnly' => false, 'imagesMax' => $imagesMax])

    </div>
</div>

@elseif(is_array($images))

<div class="row">

@foreach($images as $tag => $data)

    <div class="col-md-4 col-lg-4">

        @include('images::gallery', ['owner' => $resource, 'viewOnly' => false, 'tag' => $tag, 'title' => $data['title'], 'imagesMax' => $data['imagesMax']])

    </div>

@endforeach

</div>

@endif
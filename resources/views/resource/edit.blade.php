@extends('admin.layout')

@section('head_scripts')

    <script type="text/javascript" src="/packages/lampacore/content/js/ckeditor/ckeditor.js"></script>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css">
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
    
    <script type="text/javascript">
        $(function(){
    
            $('#date').datetimepicker({
                format: "Y-m-d H:i"
            });
            
            
        });
    </script>
    
@stop

@section('breadcrumbs')
    @if(!$group->get('single'))


        <?php $fg = $resource->parentChain(1)->first()->group() ?>

        @foreach($resource->parentChain() as $parent)


            @if($childSlug = $parent->group()->firstChildSlug())
              <li>
                  {!! link_to_action('\Lampacore\Content\ResourceController@getList', $parent->title(), [$childSlug, $parent->id]) !!}
               <a href="{{action('\Lampacore\Content\ResourceController@getEdit', $parent->id)}}"><span class="glyphicon glyphicon-edit"></span></a>
              </li>
            @endif

        @endforeach


    @endif

    @if($resource->exists)
        <li>{!!link_to(URL::current(), $resource->title())!!}</li>
    @else
        <li>{!!link_to(URL::current(), $group->get('name'))!!}</li>
    @endif

@stop


@section('content')


    @if($resource->parent())
        <h3 class="text-muted">{{$resource->parent()->name}}</h3>
    @endif

    <h1>

        @if($resource->exists)
            {{$resource->title()}}
        @else
            {{$group->get('name')}}
        @endif

    </h1>

    <hr />


    @if($group->get('images'))

        @include('content::resource._images', ['images'=>$group->get('images'), 'imagesMax' => $group->get('imagesMax')])

        <hr />

    @endif


    <form action="{{ action('\Lampacore\Content\ResourceController@postEdit', [$resource->id]) }}" method="post">
        <div class="row">

            <div class="col-md-6 col-lg-8">

                <input type="hidden" name="group_sign" value="{{$resource->group_sign}}" />


                @if($group->hasField('name'))
                    <div class="form-group">
                        <label for="name">{{$group->fieldName('name', 'Название')}}</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{inputValue('name', $resource)}}">
                    </div>
                @endif

                @if($group->hasField('date'))

                    <div class="form-group">
                        <label for="date">{{$group->fieldName('date', 'Дата (ГГ-ММ-ДД ЧЧ:ММ:СС)')}}</label>

                        <input type="text" class="form-control" id="date" name="date" placeholder="" value="{{$resource->date ? $resource->date->format('Y-m-d H:i') : null }}">
                    </div>

                @endif


                @if($group->hasField('slug') || ( !$resource->exists && $group->get('single')) )
                    <div class="form-group">
                        <label for="slug">{{$group->fieldName('slug', 'Slug (ссылка)')}}</label>

                        <input type="text" class="form-control" id="slug" name="slug" placeholder="" value="{{inputValue('slug', $resource)}}">
                    </div>
                @endif


                @if($group->hasField('link'))
                    <div class="form-group">
                        <label for="link">{{$group->fieldName('link', 'Внешняя ссылка')}}</label>

                        <input type="text" class="form-control" id="link" name="link" placeholder="" value="{{inputValue('link', $resource)}}">
                    </div>
                @endif


                @if($group->hasField('exc'))
                    <div class="form-group">
                        <label for="exc">{{$group->fieldName('exc', 'Выдержка')}}</label>

                        <textarea class="form-control" id="exc" name="exc" placeholder="Выдержка">{{inputValue('exc', $resource)}}</textarea>
                    </div>
                @endif


                {{--@hook('content::resource.edit', compact('resource','group'))--}}

                @if($group->hasField('content'))
                    <div class="form-group">
                        <label for="content">{{$group->fieldName('content', 'Содержание')}}</label>

                        <textarea class="form-control ckeditor" id="content" name="content" placeholder="Содержание">{{inputValue('content', $resource)}}</textarea>
                    </div>
                @endif


                @if($group->hasField('content2'))
                    <div class="form-group">
                        <label for="content">{{$group->fieldName('content2', 'Содержание 2')}}</label>

                        <textarea class="form-control ckeditor" id="content" name="content2" placeholder="Содержание">{{inputValue('content2', $resource)}}</textarea>
                    </div>
                @endif


                @if($group->hasField('content3'))
                    <div class="form-group">
                        <label for="content">{{$group->fieldName('content3', 'Содержание 3')}}</label>

                        <textarea class="form-control ckeditor" id="content" name="content3" placeholder="Содержание">{{inputValue('content3', $resource)}}</textarea>
                    </div>
                @endif


                @if($group->hasField('content4'))

                    <div class="form-group">
                        <label for="content">{{$group->fieldName('content4', 'Содержимое')}}</label>

                        <textarea rows="30" class="form-control  ckeditor" id="content" name="content4" placeholder="Содержимое">{{inputValue('content4', $resource)}}</textarea>
                    </div>
                @endif

                @if($group->get('extras'))
                    @include('extras::form', ['object' => $resource, 'key' => "content::groups.{$resource->group_sign}.extras"])
                @endif
                <button class="btn btn-primary">Сохранить</button>
            </div>

            <div class="col-md-6 col-lg-4">

                {{--@hook('content::resource.edit.right.top', compact('resource','group'))--}}


                @if($group->get('help'))
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include($group->get('help'))
                        </div>
                    </div>
                @endif

                {{--parents --}}

                @if($group->get('parent') && !$group->get('hide_parent_select'))

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$group->parent()->get('name')}}</div>
                        <div class="panel-body">

                            @foreach($group->parent()->resources(true) as $pRes)

                                <label for="parent{{$pRes->id}}">
                                    {!!Form::radio('parent_id', $pRes->id, ($resource->parent_id == $pRes->id), ['id' => 'parent'.$pRes->id])!!} {{$pRes->name}}
                                </label><br />

                            @endforeach

                        </div>
                    </div>

                @endif

                {{--/parents --}}

                {{--related--}}

                @if($group->get('related'))

                    @foreach($group->related() as $relGroup)

                        <div class="panel panel-default">
                            <div class="panel-heading">{{$relGroup->get('name')}}</div>
                            <div class="panel-body">

                                @foreach($relGroup->resources(true) as $relRes)

                                    <label for="rel{{$relRes->id}}">
                                        {!!Form::checkbox('related[]', $relRes->id, ($resource->related($relGroup->getSlug(), true)->contains($relRes)), ['id' => 'rel'.$relRes->id])!!} {{$relRes->name}}
                                    </label><br />



                                @endforeach

                            </div>

                        </div>

                    @endforeach
                @endif

                {{--/related--}}

            </div>

        </div>
    </form>


@stop

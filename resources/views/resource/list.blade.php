@extends('admin.layout')

@section('breadcrumbs')

    @if($parentResource->exists)


        <?php $fg = $parentResource->parentChain(1)->first()->group() ?>

        <li>{!! link_to_action('\Lampacore\Content\ResourceController@getList', $fg->get('name_plural'), [$fg->getSlug()]) !!}</li>

        @foreach($parentResource->parentChain(true) as $parent)

            @if($childSlug = $parent->group()->firstChildSlug())
                <li>
                    {!! link_to_action('\Lampacore\Content\ResourceController@getList', $parent->title(), [$childSlug, $parent->id]) !!}
                    <a href="{{action('\Lampacore\Content\ResourceController@getEdit', $parent->id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                </li>
            @endif

        @endforeach
        {{--@crumb($group->get('name_plural'), '\Lampacore\Content\ResourceController@getList', [$group->getSlug(), $parentResource->id])--}}

    @else

        {{--@crumb($group->get('name_plural'), 'Content\ResourceController@getList', [$group->getSlug()])--}}
        <li>{!! link_to_action('\Lampacore\Content\ResourceController@getList', $group->get('name_plural'), [$group->getSlug()]) !!}</li>



    @endif



@stop

@section('content')
    <script type="text/javascript">
        var tableForSort = '{{ (new Lampacore\Content\Resource)->getTable() }}'
    </script>
    <script type="text/javascript" src="/packages/lampacore/positions/js/sort.js"></script>

    @if($parentResource->exists)
        <h3 class="text-muted">{{$parentResource->name}}</h3>
    @endif

    <h1>
        {{$group->get('name_plural')}}
        <a href="{{ action('\Lampacore\Content\ResourceController@getCreate', [$group->getSlug(), $parentResource->id]) }}" class="btn btn-success">добавить</a>
    </h1>



    <hr />


    @if(View::exists('admin.content.'.$group->getSlug().'.list'))
        @include('admin.content.'.$group->getSlug().'.list')
    @else
        @include('content::presets.default.list')
    @endif

@stop

<?php

use Lampacore\Content\Resource;

Resource::deleting(function (Resource $res) {

    $res->deleteChilds();

});
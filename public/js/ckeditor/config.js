/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */


CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    config.height = 400;
    config.allowedContent = true;

  
    config.filebrowserImageUploadUrl = '/admin/upload/cke/image';
    config.filebrowserUploadUrl  = '/admin/upload/cke/file';
    
    config.toolbar = 'Full';
    
    config.toolbar_Full = [
	{ name: 'document', items : [ 'Source' ] },
	{ name: 'tools', items : [ 'Maximize' ] },
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
	{ name: 'styles', items : [ 'FontSize', 'TextColor' ] },
	{ name: 'paragraph', items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },	
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'links', items : [ 'Link','Unlink' ] },
	{ name: 'insert', items : [ 'Image','Table', 'Youtube' ] },

];


};

